# XMIPP STUFF
OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
OLD_PATH=$PATH
OLD_XMIPP_HOME=$XMIPP_HOME

function pathclear {
export PATH=$OLD_PATH
export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH
export XMIPP_HOME=$OLD_XMIPP_HOME
}

function pathxmipp {
pathclear
test -s ~/$1/.xmipp.bashrc && . ~/$1/.xmipp.bashrc || . ~/$1/xmipp.bashrc
}

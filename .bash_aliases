alias ..='cd ..'
alias l='ls -alF --color'
alias rehash='hash -r'
alias ssh='ssh -Y'
